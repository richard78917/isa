#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define EXAMPLE_PORT 6001
#define EXAMPLE_ADDR "127.0.0.1" /*Localhost*/

int main(int argc)
{
   struct sockaddr_in addr;
   int addrlen, sock, cnt;
   char message[50];

   /* set up socket */
   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) {
     perror("socket");
     exit(1);
   }

   memset((char*)&addr, 0, sizeof(addr));

   addr.sin_family = AF_INET;
   addr.sin_addr.s_addr = htonl(INADDR_ANY);
   addr.sin_port = htons(EXAMPLE_PORT);
   
   if (bind(sock,(struct sockaddr *)&addr, sizeof(addr))<0)
   {
       perror("failed to bind");
       return -1; 
   }

      addr.sin_addr.s_addr = inet_addr(EXAMPLE_ADDR);
      addr.sin_port = htons(8000);
      while (1) {
	 time_t t = time(0);
	 sprintf(message, "time is %-24.24s", ctime(&t));
	 printf("sending udp message: %s\n", message);
	 cnt = sendto(sock, message, sizeof(message), 0,
		      (struct sockaddr *) &addr, sizeof(addr));
	 if (cnt < 0) {
 	    perror("sendto");
	    exit(1);
	 }
	 sleep(5);
      }
}
