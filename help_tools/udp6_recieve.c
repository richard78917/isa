#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define EXAMPLE_PORT 8000

int main(int argc)
{
   struct sockaddr_in6 addr, remote_addr;
   socklen_t addrlen = sizeof(remote_addr);
   int sock, cnt;
   char message[50];

   /* set up socket */
   sock = socket(AF_INET6, SOCK_DGRAM, 0);
   if (sock < 0) {
     perror("socket");
     exit(1);
   }

   memset((char*)&addr, 0, sizeof(addr));

   addr.sin6_family = AF_INET6;
   addr.sin6_addr.s6_addr16[15] = 1;
   addr.sin6_port = htons(EXAMPLE_PORT);
   
   if (bind(sock,(struct sockaddr *)&addr, sizeof(addr))<0)
   {
       perror("failed to bind");
       return -1; 
   }

      while (1) {

	 cnt = recvfrom(sock, message, 50, 0,
		      (struct sockaddr *) &remote_addr, &addrlen);
	 printf("Recieved udp message: %s\n", message);
	 if (cnt < 0) {
 	    perror("sendto");
	    exit(1);
	 }
	 sleep(5);
      }
}
