# Makefile
#
#
# Definice promennych
CC=gcc
CFLAGS=-std=gnu11 -g
RM=rm -f

#
#
# Seznam vytvorenych souboru
all: m2u 

#
#
# GCC a zavislosti pro interpret
m2u: m2u.c
	$(CC) $(CFLAGS) m2u.c  -o $@


# clean parametr makra, pro smazani prekladacem vytvorenych souboru
.PHONY: clean

clean:
	$(RM) m2u 
