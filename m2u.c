#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>

#define BUFFER_SIZE 4096


typedef enum types {
	IP4,
	IP6,
	HOST,
	UNDEF
} Types;



typedef struct options {
	char *source;		/*Zdrojova adresa */
	char *target;		/*Cielova adresa */
	Types src_type;		/*typ zdroju*/
	Types target_type;	/*typ cielu*/
	int src_port;
	int target_port;
} Options;

/*Funkcia na odchytenie signalov*/
void sig_handler(int signo);

int host_parse(char *ip_string, char t);
void print_opts(Options opts);
int args_parse(int argc, char *argv[]);
void error_handle(int err_no);
int ip_parse(char *ip_string, char t);
void print_help();
int ipv6_parse(char *ip_string, char t);
int ipv4_parse(char *ip_strin, char t);
char hexa_conv(char hexa);
char *ip_create(char *ip, char *ip_second, int i, int j);

void resolve_host();

void run();

void ipv6_set(uint8_t * ipv6_addr, char *my_addr);
void ipv4_multi_socket();
void ipv6_multi_socket();

void ipv4_uni_socket();
void ipv6_uni_socket();

void create_sockets();

void mem_clean();

/*global variables*/
int multicast_socket; /*socket descriptors*/
int unicast_socket;

struct sockaddr_in unicast_addr; /*Target addres structure*/
struct sockaddr_in6 unicast6_addr;

Options opts;			/*GLOBAL OPTS VARIABLE */

int main(int argc, char *argv[])
{

	/*Register signal handler */
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		error_handle(-255);
	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		error_handle(-255);
	/*Initialize options structure */
	opts.src_type = UNDEF;
	opts.target_type = UNDEF;

	if (args_parse(argc, argv) != 0)
		error_handle(-1);

//print_opts(opts);
	resolve_host();
//print_opts(opts);

	create_sockets();
	/*Pociatocna konfiguracia hotova, mozme prenasat data */

	run();

//    print_opts(opts);
	mem_clean();
	return 0;
}


/*Funkcia sa pokusi prelozit hostname na ip addresu*/
/*Ako ip adresa sa pouzije prva moznost*/
void resolve_host()
{
	struct addrinfo hints, *p;
	int status;
	char ipstr[INET6_ADDRSTRLEN];
	void *addr;
	char *ipver;

	if (opts.target_type != HOST) { 
                /*Nie je treba prekladat hostname*/
                /*v argumentoch je ip addresa zadana*/
		return;
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; /*ipv6 alebo ipv4*/
	hints.ai_socktype = SOCK_STREAM;

	status = getaddrinfo(opts.target, NULL, &hints, &p);

	if (status != 0) { /*Chyba pri preklade domeny*/
		error_handle(-5);

	}

	if (p->ai_family == AF_INET) {	// IPv4
		struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
		addr = &(ipv4->sin_addr);
		opts.target_type = IP4;
	} else {		// IPv6
		struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
		addr = &(ipv6->sin6_addr);
		opts.target_type = IP6;
	}

	inet_ntop(p->ai_family, addr, ipstr, sizeof(ipstr));
	free(opts.target);
	opts.target = malloc(sizeof(char) * sizeof(ipstr) + 1); /*miesto na ulozenie adresy*/
	memset(opts.target, 0, sizeof(ipstr));
	strncpy(opts.target, ipstr, strlen(ipstr));

	freeaddrinfo(p);

}

/*Funkcia sluzi na samotne preposielanie paketov*/
void run()
{

	char buffer[BUFFER_SIZE];
	int read_cnt, sent_cnt;

	while (1) {
		/*Jednoduchy nekonecny cyklus*/
		read_cnt = read(multicast_socket, buffer, BUFFER_SIZE); /*Nacitaj data*/
		if (read_cnt < 0) {
			error_handle(-10);
		}

		if (opts.target_type==IP4)
		{ /*ipv4*/
		sent_cnt =
		    sendto(unicast_socket, buffer, read_cnt, 0, /*Odosli data*/
			   (struct sockaddr *)&unicast_addr,
			   sizeof(unicast_addr));
		}
		else
		{ /*ipv6*/
		sent_cnt =
		    sendto(unicast_socket, buffer, read_cnt, 0, /*Odosli data*/
			   (struct sockaddr *)&unicast6_addr,
			   sizeof(unicast6_addr));
		
		}

		if (sent_cnt < 0) { /*Vyskytla sa chyba pri odosielani*/
			error_handle(-20);
		}

	}

}


/*Funkcia vytvori sockety na komunikaciu a nabinduje ich na proces*/
void create_sockets()
{
	/*First create multicast socket */
	if (opts.src_type == IP4)
		ipv4_multi_socket();

	if (opts.src_type == IP6)
		ipv6_multi_socket();

	/*Create socket for unicast communication*/

	if (opts.target_type == IP4)
		ipv4_uni_socket();

	if (opts.target_type == IP6)
		ipv6_uni_socket();

}


/*Vytvori ip socket na unicast komunikaciu a vsetko potrebne k tomu*/
void ipv4_uni_socket()
{

	struct sockaddr_in addr;
	int i;
	/*Vytvorime socket*/
	unicast_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (unicast_socket < 0) {
		error_handle(-2);
	}

	memset((char *)&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	for (i = 1500; i <= 60000; i++) { /*Pokusime sa najst volny port*/
		addr.sin_port = htons(i);
		if (bind(unicast_socket, (struct sockaddr *)&addr, sizeof(addr))
		    < 0) {
			continue;
		} else {
			break;
		}
	}

	if (i == 60000) {	/*Chyba nebude v obsadenom porte */
		error_handle(-3);
	}

	memset((char *)&unicast_addr, 0, sizeof(unicast_addr));


	/*Nakonfigurujeme adresu vzdialeneho socketu*/
	unicast_addr.sin_family = AF_INET;
	unicast_addr.sin_addr.s_addr = inet_addr(opts.target);
	unicast_addr.sin_port = htons(opts.target_port);

}



void ipv6_uni_socket()
{
	struct sockaddr_in6 addr;
	int addrlen;
	int i;
	/*Vytvorime socket*/
	unicast_socket = socket(AF_INET6, SOCK_DGRAM, 0);
	if (unicast_socket<0)
        {
		error_handle(-2);
        }

	memset((char*)&addr, 0, sizeof(addr));

	addr.sin6_family = AF_INET6;
	addr.sin6_addr = in6addr_any;

	for (i = 1500; i <= 60000; i++) { /*Najdeme volny port*/
		addr.sin6_port = htons(i);
		if (bind(unicast_socket, (struct sockaddr *)&addr, sizeof(addr))
		    < 0) {
			continue;
		} else {
			break;
		}
	}

	if (i == 60000) {	/*Chyba nebude v obsadenom porte */
		error_handle(-3);
	}

	/*Nakonfigurujeme adresu vzdialeneho socketu*/
	memset((char *)&unicast6_addr, 0, sizeof(unicast6_addr));
	unicast6_addr.sin6_family = AF_INET6;
	unicast6_addr.sin6_port = htons(opts.target_port);

	inet_pton(AF_INET6, opts.target, &(unicast6_addr.sin6_addr));

}

void ipv6_multi_socket()
{
	struct sockaddr_in6 addr;
	struct ipv6_mreq mreq;
	socklen_t addrlen = sizeof(addr);

	/*Vytvorime socket*/
	multicast_socket = socket(AF_INET6, SOCK_DGRAM, 0);
	if (multicast_socket < 0) {
		perror("socket");
		error_handle(-2);
	}

	memset((char *)&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	addr.sin6_addr = in6addr_any;
	addr.sin6_port = htons(opts.src_port);

	if (bind(multicast_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		error_handle(-2);
	}

	inet_pton(AF_INET6,opts.source,&(mreq.ipv6mr_multiaddr));
	mreq.ipv6mr_interface = 0; /*Default multicast interface*/

	/*Vstup do IGMP skupiny*/
	if (setsockopt(multicast_socket,IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof(mreq))<0)
	{
	error_handle(-4);
	}
	
}

void ipv4_multi_socket()
{
	struct sockaddr_in addr;
	int addrlen;
	struct ip_mreq mreq;

	/*Vytvorime socket*/
	multicast_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (multicast_socket < 0) {
		error_handle(-2);
	}

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(opts.src_port);
	addrlen = sizeof(addr);

	if (bind(multicast_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		error_handle(-3);
	}

	/*Prihlasenie do multicastovej skupiny*/
	mreq.imr_multiaddr.s_addr = inet_addr(opts.source); /*konfiguracia struktur*/
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(multicast_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, /*samotne prihlasenie*/
		       &mreq, sizeof(mreq)) < 0) {
		error_handle(-4);
	}

}

int args_parse(int argc, char *argv[])
{
	/*Ulozenie adries */
	char *target = NULL, *source = NULL;
	/*Print help */
	if (argc == 2) {
		if (strcmp(argv[1], "-h") == 0) {
			print_help();
			exit(0);
		}
	}

	if (argc != 5)
		return -1;	/*Nespravny pocet argumentov */

	if ((strcmp(argv[1], "-g") == 0) && (strcmp(argv[3], "-h") == 0)) {
		source = argv[2];
		target = argv[4];
	}

	if ((strcmp(argv[1], "-h") == 0) && (strcmp(argv[3], "-g") == 0)) {
		target = argv[2];
		source = argv[4];
	}

	if (target == NULL)
		return -1;

	if (ip_parse(target, 't') != 0)
		return -1;	/*cielova IP */
	if (ip_parse(source, 's') != 0)
		return -1;	/*Zdrojova IP */

	return 0;
}

int ip_parse(char *ip_string, char t)
{
	if (ip_string[0] == '[') {	/*IPv6 address */
		ipv6_parse(ip_string, t);
		return 0;
	}
	if (ipv4_parse(ip_string, t) == 0) {
		return 0;
	}
	if (host_parse(ip_string, t) == 0) {
		return 0;
	}
	return -1;
}


/*Funkcia spracuje ipv6 adresu*/
int ipv6_parse(char *ip_string, char t)
{
	char ipv6[32] = { 0 };	/*Prva cas IPv6 pred :: */
	int ip_counter = 0;
	char ipv6_second[32] = { 0 };	/*Druha cast IPv6 za :: */
	int ip2_counter = 0;
	char help_array[4] = { 0 };
	char part_using = 1;    /*Urcuje ci spracuvavame cast pred :: lebo za*/
	int port = -1;		/*cislo portu */

	memset(ipv6, 48, 32);
	memset(ipv6_second, 48, 32);

	int i = 1, j = 0, a = 3;
	while (i < strlen(ip_string)) {
		j = 0;

		if ((ip_string[i] == ':') && (ip_string[i + 1] == ':')) {	/*narazili sme na postupnost nul */
			if (part_using == 2)
				error_handle(-1);	/*Postupnost moze byt iba jedna */
			part_using = 2;
			i += 2;
			continue;
		}

		if (ip_string[i] == ':') {	/*oddelenie ipv6 casti */
			hexa_conv(ip_string[i + 1]);	/*znak : musi byt nasledovany validnym hexa znakom */
			i++;
			continue;
		}

		if (ip_string[i] == ']') {
			i++;
			break;
		}

		memset(help_array, 48, 4);
		if (part_using == 1) {
			while ((ip_string[i] != ':') &&
			       (ip_string[i] != ']') && (i < strlen(ip_string)))
			{
				if (j > 3) {
					error_handle(-1);
				}

				help_array[j] = hexa_conv(ip_string[i]);
				i++;
				j++;
			}
			i--;
			j--;
			a = 3;
			while (j >= 0) {
				ipv6[ip_counter + a] = help_array[j];
				a--;
				j--;
			}
			ip_counter += 4;
		} else {
			while ((ip_string[i] != ':') &&
			       (ip_string[i] != ']') && (i < strlen(ip_string)))
			{
				if (j > 3) {
					error_handle(-1);
				}

				help_array[j] = hexa_conv(ip_string[i]);
				i++;
				j++;
			}
			i--;
			j--;
			a = 3;
			while (j >= 0) {
				ipv6_second[ip2_counter + a] = help_array[j];
				a--;
				j--;
			}
			ip2_counter += 4;
		}

		i++;

	}

	if (ip_string[i] != ':')
		error_handle(-1);

	port = atoi(ip_string + i + 1);
	if (t == 't') {
		opts.target =
		    ip_create(ipv6, ipv6_second, ip_counter, ip2_counter);
		opts.target_type = IP6;
		opts.target_port = port;
	} else {
		opts.source =
		    ip_create(ipv6, ipv6_second, ip_counter, ip2_counter);
		opts.src_type = IP6;
		opts.src_port = port;
	}

}


/*Pomocna funkcia ktora zlozi IP adresu z 2 casti
  pri com medzi casti vlozi 000000              */
char *ip_create(char *ip, char *ip_second, int i, int j)
{
	char *result;
	result = (char *)malloc(sizeof(char) * 32 + 1 + 7);
	memset(result, '0', 39);
	result[39] = 0;
	char help[32];
	memset(help, '0', 32);
	result[32] = 0;
	int a = 0, b = 0, c = 4;

	/*Kontrolne pocty*/
	if ((i == 32) && (j != 0))
		error_handle(-1);
	if (((i + j) > 28) && (j != 0))
		error_handle(-1);
	if ((j == 0) && (i != 32))
		error_handle(-1);

	memcpy(help, ip, i); /*Prva cast*/
	memcpy(help + 32 - j, ip_second, j); /*Druha cast*/

	for (a = 0; a < 39; a++) {
		if (a == c) {
			result[c] = ':';
			c += 5;
		} else {
			result[a] = help[b];
			b++;
		}
	}

	return result;
}


/*Spracovanie ipv4 adresy*/
int ipv4_parse(char *ip_string, char t)
{
	char buff[15] = { 0 };
	int buff_cnt = 0;
	char byte_buff[4] = { 0 };
	int byte_buff_cnt = 0;
	int i = 0, j = 0;
	int port;

	for (int k = 0; k < 15; k++)
		buff[k] = 0;

	while (i < 4) {
		while ((ip_string[j] != '.') && (ip_string[j] != ':')) {
			if (((ip_string[j] < '0') || (ip_string[j] > '9'))	/*Neplatny znak v ipv4 */
			    ||(byte_buff_cnt > 2) || (j > (strlen(ip_string)))) {
				return -1;	/*Neplatny znak v ipv4 */
			}

			buff[buff_cnt] = byte_buff[byte_buff_cnt] =
			    ip_string[j];
			buff_cnt++;
			byte_buff_cnt++;

			j++;
		}

		if (byte_buff_cnt == 0)
			return -1;

		byte_buff[byte_buff_cnt] = 0;
		if (atoi(byte_buff) > 255)
			return -1;

		if (ip_string[j] == '.') {
			buff[buff_cnt] = ip_string[j];
			buff_cnt++;
			j++;
		}

		byte_buff_cnt = 0;
		if (atoi(byte_buff) > 255) {	/*viac nez 255 v jednom byte */
			return -1;
		}

		i++;
	}

	if (ip_string[j++] != ':')
		return -1;
	i = j;
	if (j == strlen(ip_string)) {
		return -1;
	}

	while (j < strlen(ip_string)) {
		if ((ip_string[j] < '0') || (ip_string[j] > '9')) {
			return -1;
		}
		j++;
	}

	port = atoi(ip_string + i);
	/*Ip adresa je v tomto okamihu zpracovana je treba ju ulozit do struktury */
	if (t == 's') {		/*Source */
		opts.src_type = IP4;
		opts.src_port = port;
		opts.source = (char *)malloc(i * sizeof(char));
		memcpy(opts.source, buff, i - 1);
		opts.source[i - 1] = 0;	/*Ukoncovacia nula stringu */
	} else {		/*Target */

		opts.target_type = IP4;
		opts.target_port = port;
		opts.target = (char *)malloc(i * sizeof(char));
		memcpy(opts.target, buff, i - 1);
		opts.target[i - 1] = 0;	/*Ukoncovacia nula stringu */

	}

	return 0;
}

int host_parse(char *ip_string, char t)
{
	char *port_ptr = strchr(ip_string, ':');
	int host_length;
	int j;
	int port;

	if (port_ptr == NULL)
		error_handle(-1);
	host_length = (int)(port_ptr - ip_string);
	if (host_length < 1)
		return -1;

	j = host_length + 1;
	while (j < strlen(ip_string)) {
		if ((ip_string[j] < '0') || (ip_string[j] > '9')) {
			return -1;
		}
		j++;
	}
	port = atoi(port_ptr + 1);

	if (t == 's') {		/*Source multicast group address cant be domain name */
		error_handle(-1);
	} else {
		opts.target_type = HOST;
		opts.target_port = port;
		opts.target = malloc(sizeof(char) * (host_length + 1));
		memcpy(opts.target, ip_string, host_length);
		opts.target[host_length] = 0;
	}

	return 0;
}

/*Pomocna funkcia pouzita pri spracovani ipv6
 adresy, prakticky nam len povie ci sa jedna o platny
 znak ipv6 adresy
*/
char hexa_conv(char hexa)
{

	if (((hexa >= '0') && (hexa <= '9')) || (hexa >= 'a') && (hexa <= 'f')) {
		return hexa;
	} else {
		error_handle(-1);
	}

}

/*Funkcia urcena na odchytenie a vypis chyby*/
void error_handle(int err_no)
{
	mem_clean();
	switch (err_no) {
	case -1:
		fprintf(stderr, "Arguments parsing error.\nTry -h for help\n");
		break;

	case -2:
		fprintf(stderr, "Can't create socket\n");
		break;

	case -3:
		fprintf(stderr, "Error binding socket\n");
		break;

	case -4:
		fprintf(stderr, "Error adding membership\n");
		break;

	case -5:
		fprintf(stderr, "Could not resolve hostname\n");
		break;

	case -10:
		fprintf(stderr, "Error on reading data from multicast group\n");
		break;

	case -20:
		fprintf(stderr, "Error on sending data to unicast host\n");
		break;

	case (-100):
		fprintf(stderr, "Not implemented yet\n");
		break;

	default:
		fprintf(stderr, "Unknown error.\n");
		break;
	}
	exit(err_no);
}


/*Funkcia uvolni pridelenu pamat
  zatvori sockety a tym sa zbavi
  pripadneho clenstva v multicast skupine*/
void mem_clean()
{
	if (opts.src_type != UNDEF)
		free(opts.source);

	if (opts.target_type != UNDEF)
		free(opts.target);
	close(multicast_socket);
	close(unicast_socket);

}

void ipv6_set(uint8_t * ipv6_addr, char *my_addr)
{
	char buff[3];
	buff[2] = 0;
	for (int i = 0; i < 16; i++) {
		buff[0] = my_addr[i * 2];
		buff[1] = my_addr[(i * 2) + 1];
		ipv6_addr[i] = (uint8_t) strtol(buff, NULL, 16);
	}
}


/*FUNKCIA URCENA NA TESTOVANIE*/
/*VYPISE HODNOTY MOZNOSTI SPUSTENIA*/
void print_opts(Options opts)
{
	printf("TESTING PRINT\n");
	printf("-----SOURCE-------\n");
	printf("TYP: ");
	if (opts.src_type == IP4)
		printf(" IPv4\n");
	if (opts.src_type == IP6)
		printf(" IPv6\n");
	if (opts.src_type == HOST)
		printf(" HOST\n");
	if (opts.src_type == UNDEF) {
		printf(" UNDEF\n");
	} else {
		printf("%s\n", opts.source);
	}

	printf("-----TARGET-------\n");
	printf("TYP: ");
	if (opts.target_type == IP4)
		printf(" IPv4\n");
	if (opts.target_type == IP6)
		printf(" IPv6\n");
	if (opts.target_type == HOST)
		printf(" HOST\n");
	if (opts.target_type == UNDEF) {
		printf(" UNDEF\n");
	} else {
		printf("%s\n", opts.target);
	}
	printf("END OF TESTING PRINT\n");
}

/*Spracovanie signalov*/
void sig_handler(int signo)
{
	if ((signo == SIGINT) || (signo == SIGTERM))
		printf("TERMINATING PROCESS\n");

	mem_clean();
	exit(0);

}

/*Vypis napovedy*/
void print_help()
{
	printf("Brno University of Technology\n");
	printf("Faculty of Information Technology\n");
	printf("Author: Richard Wolfert, xwolfe00\n");
	printf("Email: xwolfe00@stud.fit.vutbr.cz\n");
	printf("Multicast to unicast utillity\n");

	printf("Moznosti spustenia:\n");
	printf("./m2u [-g multicast group address:port] [-h host:port]\n");
	printf("Priklady:\n");
	printf("./m2u -g 239.0.0.1:1234 -h [::1]:5000\n");
	printf("./m2u -g [FF02::1]:1234 -h eva6.fit.vutbr.cz:5000\n");
	printf("./m2u -h 192.168.11.8:34000 -g 239.0.0.1:2000\n");
}
